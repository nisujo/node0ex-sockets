var socket = io();

socket.on('connect', function() {

    console.log('Connected to server.');

});

socket.on('disconnect', function() {

    console.log('Connection with server ended');

});

socket.on('send-message', function(message) {

    console.log(message);

});

document.getElementById('send-message').addEventListener('click', function(e) {

    let data = {
        user: document.getElementById('name').value,
        message: document.getElementById('message').value
    }

    socket.emit('send-message', data, function(res) {

        // Callback que se ejecuta al terminar el envío.
        console.log('response server', res);

    });

});