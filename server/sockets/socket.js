const { io } = require('../server');

io.on('connection', (client) => {

    console.log('User Connected');

    client.emit('send-message', {
        message: 'Welcome to this server'
    });

    client.on('disconnect', () => {

        console.log('User disconnected');

    });

    client.on('send-message', (message, callback) => {

        console.log(message);
        client.broadcast.emit('send-message', message);

        // if (message.user) {

        //     callback({ message: 'All right' });

        // } else {

        //     callback({ message: 'All wrong' });

        // }

    });

});